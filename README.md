# Rke2 In Docker

Preload rancher images and binaries in docker, in order to be able to deploy rke2 clusters in CI using cluster-api-rke2 boostrap provider and cluster-api-docker infrasture provider.

The pipeline is configured to build images containing RKE2 releases matching tags in this repository.

For example, adding a tag `v1.24.4+rke2r1` will produce image `registry.gitlab.com/t6306/components/docker-images/rke2-in-docker:v1-24-4-rke2r1` including correponding rke2 binary and images

