FROM kindest/node:v1.31.4

ARG INSTALL_RKE2_ARTIFACT_PATH=/opt/rke2-artifacts
# renovate: datasource=github-releases depName=rancher/rke2
ARG RKE2_VERSION=v1.31.5+rke2r1

RUN mkdir -p ${INSTALL_RKE2_ARTIFACT_PATH} \
 && curl -OLs https://github.com/rancher/rke2/releases/download/${RKE2_VERSION}/rke2-images.linux-amd64.tar.zst --output-dir ${INSTALL_RKE2_ARTIFACT_PATH} \
 && curl -OLs https://github.com/rancher/rke2/releases/download/${RKE2_VERSION}/rke2.linux-amd64.tar.gz --output-dir ${INSTALL_RKE2_ARTIFACT_PATH} \
 && curl -OLs https://github.com/rancher/rke2/releases/download/${RKE2_VERSION}/sha256sum-amd64.txt --output-dir ${INSTALL_RKE2_ARTIFACT_PATH} \
 && curl -sfL https://get.rke2.io --output /opt/install.sh

EXPOSE 6443/tcp 9345/tcp
